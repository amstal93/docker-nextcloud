[![pipeline status](https://gitlab.com/deixismou/docker-nextcloud/badges/master/pipeline.svg)](https://gitlab.com/deixismou/docker-nextcloud/commits/master)

![logo](logo.png)

# About this repo

> *NOTE:* Alpine-based docker images [have now been implemented] in the official
> repo as of March 2018. Therefore, it is recommended that you use the official
> images provided there.

This is a fork of the official Docker images for [NextCloud] which provides
additional images based on Alpine Linux---a security-oriented, lightweight Linux
distribution which is an ideal foundation for micro-service containers.

[have now been implemented]: https://github.com/nextcloud/docker/pull/259
[NextCloud]: https://nextcloud.com/
[Alpine Linux]: https://www.alpinelinux.org/

# What is NextCloud?

A safe home for all your data. Access & share your files, calendars, contacts,
mail and more from any device, on your terms.  NextCloud is an open source,
self-hosted file share and communication platform.  It is functionally similar
to Dropbox, although NextCloud is free and open-source, allowing anyone to
install and operate it on a private server.

> [nextcloud.com/](https://nextcloud.com/)
>
> [wikipedia.org/wiki/Nextcloud](https://en.wikipedia.org/wiki/Nextcloud)

# How to use this image

This image is designed to be used in a micro-service environment. It consists of
the NextCloud installation in an [php-fpm](https://hub.docker.com/_/php/)
container. To use this image it must be combined with any web-server that can
proxy the HTTP requests to the FastCGI-port of the container. The unofficial
images of Nginx [found here] have been modified to work with php-fpm, and are an
ideal companion to the Alpine-based images in this repo.

For detailed instructions on using these images, please refer to the [official
repo] or the [Docker Hub page].

[found here]: https://gitlab.com/deixismou/docker-nginx
[official repo]: https://github.com/nextcloud/docker
[Docker Hub page]: https://hub.docker.com/_/nextcloud/
